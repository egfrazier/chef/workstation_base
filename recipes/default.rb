#
# Cookbook:: workstation_base
# Recipe:: default
#
# Copyright:: 2020, Elizabeth Frazier, All Rights Reserved.

# SYSTEM CORE
pacman_package 'openvpn' do
  package_name 'openvpn'
end

pacman_package 'emacs' do
  package_name 'emacs'
end

pacman_package 'rsync' do
  package_name 'rsync'
end

# DEVELOPMENT ENVIRONMENT

pacman_package 'python' do
  package_name 'python'
end

# INFRASTRUCTURE MANAGEMENT

pacman_package 'terraform' do
  package_name 'terraform'
end

pacman_package 'vault' do
  package_name 'vault'
end

pacman_package 'aws-cli' do
  package_name 'aws-cli'
end

# GRAPHICAL ENVIRONMENT

pacman_package 'xorg-server' do
  package_name 'xorg-server'
end

pacman_package 'xorg-xinit' do
  package_name 'xorg-xinit'
end

pacman_package 'spectrwm' do
  package_name 'spectrwm'
end

pacman_package 'feh' do
  package_name 'feh'
end

pacman_package 'picom' do
  package_name 'picom'
end

pacman_package 'rxvt-unicode' do
  package_name 'rxvt-unicode'
end

pacman_package 'dmenu' do
  package_name 'dmenu'
end

pacman_package 'rsync' do
  package_name 'rsync'
end

pacman_package 'vimb' do
  package_name 'vimb'
end

# TEMPLATE FILES
template '/etc/openvpn/client.rb' do
  source 'openvpn/client.erb'
  mode '0644'
  owner 'root'
  group 'root'
  variables(vpn_host: node['vpn_host'])
end
